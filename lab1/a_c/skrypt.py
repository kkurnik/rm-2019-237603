import random
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch


delta = 0.1
x_size = 10
y_size = 10
obst_vect = [(random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10))]

#obst_vect = [(random.randrange(-10,10),random.randrange(-10,10))]

start_point=(-10,random.randrange(-10,10))
finish_point=(10,random.randrange(-10,10))

k0 = 1
k0i = 1.7
d0 = 20
kp = 3

#Potencjal przyciagajacy
def Odl(p1,p2):
	tmp_local = np.subtract(p1,p2)
	return np.sqrt((tmp_local[0]**2) + (tmp_local[1]**2))

#Up=1/2*kp*(Odl(start_point,finish_point))**2

'''
if Odl(start_point,obst_vect) <= d0 :
	Vo = 1/2*k0i*(1/Odl(start_point,obst_vect)-1/d0)**2
elif Odl(start_point,obst_vect) > d0 :
	Vo = 0
'''

#start_point,obst_vect
def Fo(p1,p2):
	if Odl(p1,p2) < d0 :
		return -k0i*(1/Odl(p1,p2)-1/d0)*(1/(Odl(p1,p2)**2))
	elif Odl(p1,p2) > d0 : 
		return 0
	elif Odl(p1,p2) == 0 :
		return 5
#Sily przyciagajace
#Fp = kp*Odl(start_point,finish_point)
def Fp(p1,p2):
	return k0*Odl(p1,p2)

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

#Tablica sil odpychajacych
def FoTab(p1,przeszkoda):
	tab = 0
	for a in przeszkoda:
		tab += Fo(p1,a)
	return tab


for i in y:
	for j in x:
		py = int(i/delta+1/delta*10)
		px = int(j/delta+1/delta*10)
		Z[py,px] = FoTab((j,i),obst_vect)+Fp((j,i),finish_point)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjałów')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
		   vmin = -1, vmax = 1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
