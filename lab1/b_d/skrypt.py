import random
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch


delta = 0.05
x_size = 10
y_size = 10
obst_vect = [(random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10)), (random.randrange(-10,10),random.randrange(-10,10))]

#obst_vect = [(random.randrange(-10,10),random.randrange(-10,10))]

start_point=(-10,random.randrange(-10,10))
finish_point=(10,random.randrange(-10,10))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
# Z = np.exp(-X**0)
Z = X + Y

k0 = 1
k0i = 10
d0 = 3.5
kp = 0.02


def Odl(p1,p2):
	tmp_local = tuple(np.subtract(p1,p2))
	return np.sqrt((tmp_local[0]**2) + (tmp_local[1]**2))

#Up=1/2*kp*(Odl(start_point,finish_point))**2

#Potencjal przyciagajacy
def Up(p1,p2):
	return 0.5*kp(Odl(p1,p2)**2)

def V(p1,p2):
    d = Odl(p1, p2)
    if d > d0:
        return 0
    else:
        return 0.5*k0i*(1/d - 1/d0)**2


#start_point,obst_vect
def Fo(p1,p2):
    d = Odl(p1,p2)
    if d > d0 : 
        return 0
    else:
        return k0i*(1/d - 1/d0) * (1/(d**2))

#Sily przyciagajace
#Fp = kp*Odl(start_point,finish_point)
def Fp(p1,p2):
	return kp*Odl(p1,p2)



#Tablica sil odpychajacych
def FoTab(p1,przeszkoda):
	tab = 0
	for a in przeszkoda:
		tab += Fo(p1,a)
	return tab


# for i in y:
# 	for j in x:
# 		py = int(i/delta+1/delta*10)
# 		px = int(j/delta+1/delta*10)
# 		Z[py,px] = FoTab((j,i),obst_vect)+Fp((j,i),finish_point)

for i, iv in enumerate(X):
    for j, jv in enumerate(Y):
        px = iv[i]
        py = jv[j]
        Z[j][i] = FoTab((px,py),obst_vect)+Fp((px,py),finish_point)

sciezka = []
pocz = start_point
i=50
while i > 0:
	i = i-1

	koniec = finish_point

	Vp_p = Fp(pocz,koniec)
	Vp = tuple(np.subtract(pocz,koniec))
	Vp = tuple(np.divide(Vp,len(Vp)))
	Vp = tuple(np.multiply(Vp,Vp_p))

	#wektor odpychający
	Vo_tmp = (0,0)

	for ob in obst_vect:
		Vo_p = Fo(pocz, ob)
		Vo = tuple(np.subtract(pocz,ob))
		
		if len(Vo) > d0:
			Vo_p = 0
		
		Vo = tuple(np.divide(Vo,len(Vo)))
		Vo = tuple(np.multiply(Vo,Vo_p))
		Vo_tmp = tuple(np.add(Vo_tmp,Vo))

	V_pom = tuple(np.add(Vp, tuple(np.multiply(Vo_tmp, -1))))
	V = tuple(np.divide(V_pom,len(V_pom)))

	dx = round(V[0])
	dy = round(V[1])

	pocz = (pocz[0]+dx/10, pocz[1]+dy/10)
	print(pocz)
	sciezka.append(pocz)


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjałów')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
		   vmin = -1, vmax = 1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
	plt.plot(obstacle[0], obstacle[1], "or", color='black')

for punkt in sciezka:
	plt.plot(punkt[1], punkt[0], "or", color='magenta', markersize=3)

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
